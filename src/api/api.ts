// Core
// import axios from 'axios';
import axios, { AxiosResponse } from 'axios';
import { ISingUp } from '../components/SingUp/config';


import { ILoginFormShape } from '../components/types';
import { ILogin, ISignUpWithToken } from '../types/auth';
import { IProfile } from '../types/IProfileModel';
import { ITagModel } from '../types/ITagModel';
import {
    ITaskRequest, ITaskResponse, ITasksResponse,
} from '../types/ITasksModel';

export const TODO_API_URL = 'https://lab.lectrum.io/rtx/api/v2/todos';


export const api = Object.freeze({
    get token() {
        return localStorage.getItem('token');
    },
    getVersion() {
        return '0.0.1';
    },
    auth: {
        async signup(userInfo: ISingUp): Promise<ISignUpWithToken> {
            const { data: newUser } = await axios.post<ISingUp, AxiosResponse<ISignUpWithToken>>(`${TODO_API_URL}/auth/registration`,
                userInfo);


            return newUser;
        },
        async login(credentials: ILoginFormShape): Promise<ILogin> {
            const { email, password } = credentials;
            const { data } = await axios.get<ILogin>(`${TODO_API_URL}/auth/login`,
                {
                    headers: {
                        Authorization: `Basic ${window.btoa(`${email}:${password}`)}`,
                    },
                });

            return data;
        },
        logout() {
            return fetch(`${TODO_API_URL}/auth/logout`, {
                method:  'GET',
                headers: {
                    Authorization: `Bearer ${api.token}`,
                },
            });
        },

    },
    profile: {
        async fetch(): Promise<IProfile> {
            const { data } = await axios.get<IProfile>(`${TODO_API_URL}/auth/profile`, {
                headers: {
                    Authorization: `Bearer ${api.token}`,
                },
            });

            return data;
        },
    },
    tags: {
        async fetch(): Promise<ITagModel[]> {
            const { data: tags } = await axios.get<ITagModel[]>(`${TODO_API_URL}/tags`, {
                headers: {
                    Authorization: `Bearer ${api.token}`,
                },
            });

            return tags;
        },
    },
    tasks: {
        async getTasks() {
            const { data: tasks } = await axios.get<ITasksResponse>(
                `${TODO_API_URL}/tasks`, {
                    headers: {
                        Authorization: `Bearer ${api.token}`,
                    },
                },
            );

            return tasks.data;
        },
        async updateTask(id: string, task: ITaskRequest) {
            const { data: newTask } = await axios.put<ITaskResponse>(
                `${TODO_API_URL}/tasks/${id}`,
                task,
                {
                    headers: {
                        Authorization: `Bearer ${api.token}`,
                    },
                },
            );

            return newTask.data;
        },


        async createTask(task: ITaskRequest) {
            const { data: newTask } = await axios.post<ITaskResponse>(
                `${TODO_API_URL}/tasks`,
                task,
                {
                    headers: {
                        Authorization: `Bearer ${api.token}`,
                    },
                },
            );

            return newTask.data;
        },

        async deleteTask(id: string) {
            const { data } = await axios.delete<void>(
                `${TODO_API_URL}/tasks/${id}`,
                {
                    headers: {
                        Authorization: `Bearer ${api.token}`,
                    },
                },
            );

            return data;
        },


    },
});
