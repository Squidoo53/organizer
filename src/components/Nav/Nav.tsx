import { NavLink } from 'react-router-dom';
import { useLogout } from '../../hooks/useLogout';

export const Nav: React.FC = () => {
    const { token, removeToken } = useLogout();
    const logout = () => removeToken();

    return (
        <nav className = 'nav'>
            { token && (
                <>
                    { !token && (<NavLink    to = '/login'>Войти</NavLink>) }
                    <NavLink  aria-disabled = { !token } to = '/task-manager'>
                К задачам
                    </NavLink>
                    <NavLink  aria-disabled = { !token } to = '/profile'>
                Профиль
                    </NavLink>
                    <button
                        className = 'button-logout'
                        onClick = { logout }>
                Выйти
                    </button>
                </>
            ) }
        </nav>
    );
};
