import styled from 'styled-components';

export interface ITaskFormControls {
    submit?: boolean;
}

export const Button = styled.button`
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 9px 19px 8px;
    border: none;
    font-size: 14px;
    text-transform: uppercase;
    font-weight: 700;
    background: none;
    color: #fff;
    border-radius: 4px;
    transition: all 0.2s;

    &:hover {
        cursor: pointer;
        transform: scale(1.1);
    }

    &:disabled {
        cursor: initial;
        transform: scale(1);
        filter: grayscale(100%);
        opacity: 0.3;
    }
`;


export const Head = styled.div`
    position: relative;
    display: flex;
    align-items: center;
    justify-content: space-between;
    height: 53px;
    margin-bottom: 27px;

    &::after {
        position: absolute;
        bottom: 0;
        display: block;
        width: 100%;
        height: 1px;
        content: "";
        background-color: #e8ecef;
    }
`;

export const CompleteTaskButton = styled.button`
    font-size: 12px;
    font-weight: 600;
    text-transform: uppercase;
    color: #778ca2;
    display: flex;
    align-items: center;
    background: none;
    border: none;
    margin-left: 26px;

    &::before {
        content: "\f14a";
        font-family: "Line Awesome Free";
        font-weight: 900;
        font-size: 19px;
        display: block;
        margin-right: 12px;
    }

    &:hover {
        cursor: pointer;
        color: #4d7cfe;
    }

    &:disabled {
        cursor: not-allowed;
        color: #778ca2;
    }

    &.completed {
        color: #4d7cfe;
    }
`;

export const RemoveTaskButton = styled.div`
    border: none;
    background: none;
    outline: none;
    margin-right: 17px;
    transition: all 200ms;

    &::before {
        content: "\f00d";
        font-family: "Line Awesome Free";
        font-weight: 900;
        font-size: 26px;
        display: block;
        color: #fe4d97;
    }

    &:hover {
        cursor: pointer;
        transform: scale(1.1);
    }
`;

export const TaskFormButton = styled(Button).attrs((props) => ({
    submit: props.type === 'submit',
}));
