import * as yup from 'yup';

export interface ITaskForm {
    title: string;
    description: string;
    tag: string;
    completed: boolean;
    deadline: Date;
}

export const schema = yup.object().shape({
    title: yup
        .string()
        .required('Минимальная длина поля title — 3'),
    deadline: yup.date()
        .nullable()
        .required('Start Date is required')
        .min(new Date(), 'Start Date must be later than today'),
    description: yup
        .string()
        .required('Минимальная длина поля description — 3'),
    tag:       yup.string().required('Выберете тег'),
    completed: yup.boolean(),
});
