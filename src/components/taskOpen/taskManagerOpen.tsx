import { yupResolver } from '@hookform/resolvers/yup';
import cx from 'classnames';
import { useForm } from 'react-hook-form';
import { useSelector } from 'react-redux';
import DatePicker, { registerLocale } from 'react-datepicker';
import { useEffect, useState, MouseEvent } from 'react';
import ru from 'date-fns/locale/ru';
import { getIsTaskFormOpen } from '../../lib/redux/selectors';
import { Input, TextArea } from './Elements/Input';
import { TaskManagerOpenWrapper } from '../styled/taskManagerOpen.styles';
import { ITaskForm, schema } from './config';
import { useTasks } from '../../hooks/useTasks';
import { useCreateTask } from '../../hooks/useCreateTask';
import { useDeleteTask } from '../../hooks/useDeleteTask';
import { useUpdateTask } from '../../hooks/useUpdateTask';
import { TaskControl } from './taskControl/taskControl';
import { Tags } from '../Tags/Tags';

registerLocale('ru', ru);

export const ManagerOpen: React.FC = () => {
    const [dataTime, setData] = useState(new Date());
    const isTaskOpen = useSelector(getIsTaskFormOpen);
    const { selectedTask } = useTasks();
    const createTask = useCreateTask();
    const deleteTask = useDeleteTask();
    const updateTask = useUpdateTask();

    const form = useForm<ITaskForm>({
        mode:          'onTouched',
        defaultValues: {
            title:       '',
            description: '',
            tag:         '',
            completed:   false,
            deadline:    new Date(),
        },
        resolver: yupResolver(schema),
    });

    const taskClasses = cx('task-card', {
        open:   isTaskOpen,
        closed: !isTaskOpen,
    });

    const errorsJSX = () => {
        const errors = Object.entries(form.formState.errors);

        if (errors.length === 0) {
            return null;
        }

        return  errors.map(([key, value]) => (
            <p key = { key } className = 'errorMessage'>{ value?.message }</p>
        ));
    };


    const setFormValues = () => {
        form.setValue('title', selectedTask?.title);
        form.setValue('deadline', new Date(selectedTask?.deadline));
        form.setValue('description', selectedTask?.description);
        form.setValue('tag', selectedTask?.tag.id);
        form.setValue('completed', selectedTask?.completed);
    };

    useEffect(() => {
        if (selectedTask) {
            form.clearErrors();
            setFormValues();
        } else {
            form.reset();
        }
    }, [selectedTask]);


    const deleteSelectedTask = async () => {
        await deleteTask.mutateAsync(selectedTask?.id);

        form.reset();
    };


    const createNewTask = async (newTask: ITaskForm) => {
        await createTask.mutateAsync({
            ...newTask,
            deadline: newTask.deadline.toISOString(),
        });
    };
    const updateSelectedTask = async (task: ITaskForm) => {
        await updateTask.mutateAsync({
            ...task,
            deadline: task.deadline.toISOString(),
        });
    };

    const completeSelectedTask = async (event: MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();

        form.setValue('completed', !selectedTask?.completed);
        await updateSelectedTask(form.getValues());

        form.reset();
    };

    const onSubmit = form.handleSubmit(async (newTask: ITaskForm) => {
        if (selectedTask) {
            await updateSelectedTask(newTask);
        } else {
            await createNewTask(newTask);
        }
        form.reset();
    });

    const resetForm = (event: MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
        form.reset();

        if (selectedTask) {
            setFormValues();
        }
    };

    const setTagValue = (id: string) => {
        form.setValue('tag', id, { shouldDirty: true, shouldValidate: true });
    };


    return (
        <div className = { taskClasses }>
            <TaskManagerOpenWrapper>
                <div>
                    <form onSubmit = { onSubmit }>
                        <TaskControl
                            selectedTask = { selectedTask }
                            handleTaskDelete = { deleteSelectedTask }
                            handleTaskComplete = { (event) => completeSelectedTask(event) } />
                        <div className = 'content'>
                            <span className = 'label'>Задача</span>
                            <Input
                                placeholder = 'Пройти интенсив по React + Redux + TS + Mobx'
                                type = 'text'
                                error = { form.formState.errors.title }
                                register = { form.register('title') } />
                            <div className = 'deadline'>
                                <span className = 'label'>Дедлайн</span>
                                <span className = 'date'>
                                    <DatePicker
                                        selected = { dataTime }
                                        onChange = { (date: Date) => {
                                            const d = new Date(date);
                                            setData(d);
                                        } }
                                        dateFormat = 'd MMM yyyy'
                                        locale = 'ru'
                                        minDate = { new Date() }
                                        startDate = { dataTime } />
                                </span>
                            </div>
                            <div className = 'description'>
                                <span className = 'label'>Описание</span>
                                <TextArea
                                    placeholder = 'После изучения всех технологий, завершить работу над проектами и найти работу.'
                                    type = 'text'
                                    tag = 'textarea'
                                    error = { form.formState.errors.description }
                                    register = { form.register('description') } />
                            </div>
                            <div className = 'tags'>
                                <Tags handleTagClick = { setTagValue } />
                            </div>
                            <div className = 'errors'>
                                {  errorsJSX() }
                            </div>
                            <div className = 'form-controls'>
                                <button
                                    type = 'reset' className = 'button-reset-task'
                                    onClick = { (event) => resetForm(event) }
                                    disabled = {
                                        !form.formState.isDirty
                                    }>Reset</button>
                                <button
                                    type = 'submit' className = 'button-save-task'
                                    disabled = {
                                        !form.formState.isDirty
                                    }>Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </TaskManagerOpenWrapper>

        </div>

    );
};
