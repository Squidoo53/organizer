import { useEffect } from 'react';
import { useTasks } from '../../hooks/useTasks';
import { ITasksModel } from '../../types/ITasksModel';
import { Preloader } from '../Preloader/Preloader';
import { Task } from './Task';


export const TasksList: React.FC = () => {
    const {
        fetchTasks, taskList, setSelectedTask, setTaskFormOpen, isTasksLoading,
    } = useTasks();

    useEffect(() => {
        fetchTasks();
    }, []);

    const handleTaskClick = (task: ITasksModel) => {
        setSelectedTask(task);
        setTaskFormOpen(true);
    };

    const taskJSX = taskList?.map((task: ITasksModel) => <Task
        key = { task.id }
        task = { task }
        handleClick = { handleTaskClick } />);

    return (
        <div className = 'tasks'>
            { isTasksLoading ? <Preloader /> : taskJSX  }
        </div>
    );
};
