import { FC } from 'react';
import { format } from 'date-fns';

import { ru } from 'date-fns/locale';
import { ITasksModel } from '../../types/ITasksModel';


interface ITaskPropsType {
    task: ITasksModel,
    handleClick: (task: ITasksModel) => void;
}

export const Task: FC<ITaskPropsType> = ({ task, handleClick }) => {
    const { title, deadline, tag } = task;
    const completed = task?.completed ? 'completed' : '';


    return (
        <div className = { `task ${completed}` } onClick = { () => handleClick(task) }>
            <div className = 'title'>{ title }</div>
            <div className = 'meta'>
                <div className = 'deadline'>{ format(new Date(deadline), 'dd MMM yyyy', { locale: ru }) }</div>

                <div
                    className = 'tag' { ...tag }
                    style = { { color: tag.color, backgroundColor: tag.bg }
                    } >{ tag.name }</div>
            </div>
        </div>
    );
};
