import { FC } from 'react';
import Loader from 'react-loader-spinner';
import { LoaderWrapper } from '../styled/loaderSpinner.styled';

export const LoaderSpinner: FC = () => {
    return (
        <LoaderWrapper>
            <Loader
                type = 'Bars'
                color = '#4D7CFE'
                height = { 100 }
                width = { 100 } />
        </LoaderWrapper>
    );
};
