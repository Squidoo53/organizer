import styled from 'styled-components';

export const TaskManagerOpenWrapper = styled.div`
.errors {
    margin-top: 25px;
    margin-bottom: 25px;

    .errorMessage {
        color: #8D211D;
        margin-bottom: 5px;
    }
}

.task-card {
    margin-top: 30px;
    width: calc(50% - 45px);
    max-height: 600px;
    margin-right: 30px;
    flex-shrink: 0;
    background-color: $color6;
    padding-bottom: 50px;

    .head {
        display: flex;
        align-items: center;
        justify-content: space-between;
        position: relative;
        height: 53px;
        margin-bottom: 27px;

        &::after {
            content: '';
            display: block;
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 1px;
            background-color: $color5;
        }

        .button-complete-task {
            font-size: 12px;
            font-weight: 600;
            text-transform: uppercase;
            color: $color17;
            display: flex;
            align-items: center;
            background: none;
            border: none;
            margin-left: 26px;

            &:disabled {
                cursor: not-allowed;
                color: $color17;
            }

            &::before {
                content: '\f14a';
                font-family: 'Line Awesome Free';
                font-weight: 900;
                font-size: 19px;
                display: block;
                margin-right: 12px;
            }

            &:hover {
                cursor: pointer;
                color: $color3;
            }

            &:disabled:hover {
                cursor: not-allowed;
                color: $color17;
            }

            &.completed {
                color: $color3;
            }
        }

        .button-remove-task {
            border: none;
            background: none;
            outline: none;
            margin-right: 17px;
            transition: all 200ms;

            &::before {
                content: '\f00d';
                font-family: 'Line Awesome Free';
                font-weight: 900;
                font-size: 26px;
                display: block;
                color: $color11;
            }

            &:hover {
                cursor: pointer;
                transform: scale(1.1);
            }
        }
    }

    .content {
        padding-left: 26px;
        padding-right: 44px;
        max-height: 596px;
        overflow-y: scroll;

        .label {
            display: block;
            font-size: 22px;
            color: $color2;
            margin-bottom: 14px;
        }

        Input {
            width: 100%;
            border: none;
            outline: none;
            color: $color4;
            font-size: 16px;
            margin-bottom: 32px;

            &::placeholder {
                color: $color2;
            }
        }

        .deadline {
            margin-bottom: 34px;

            .date {
                display: flex;
                align-items: center;

                input {
                    font-size: 16px;
                    font-weight: 600;
                    color: $color4;
                    border: none;
                    outline: none;
                }

                &::before {
                    content: '\f133';
                    font-family: 'Line Awesome Free';
                    font-weight: 900;
                    font-size: 18px;
                    margin-right: 6px;
                    color: $color17;
                    margin-bottom: 29px;
                }
            }
        }

        .description {
            margin-bottom: 32px;

            .text {
                width: 100%;
                border: none;
                resize: none;
                outline: none;
                font-size: 16px;
                line-height: 21px;
                color: $color4;

                &::placeholder {
                    color: $color2;
                }
            }
        }

        .checklist {
            margin-bottom: 34px;

            .label {
                display: block;
                font-size: 18px;
                color: $color4;
                margin-bottom: 16px;
            }
`;
