import * as yup from 'yup';
import { ILoginFormShape } from '../types';

const tooShortMessage = 'минимальная длина - 8 символов';
const tooLongMessage = 'максимальная длина - 40 символов';

export const schema: yup.SchemaOf<ILoginFormShape> = yup.object().shape({
    email: yup
        .string()
        .email()
        .required('Поле email обязательно для заполнения'),
    password: yup
        .string()
        .min(8, tooShortMessage)
        .max(16, tooLongMessage)
        .required('Поле password обязательно для заполнения'),
});

