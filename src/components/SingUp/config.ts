import * as yup from 'yup';
import { ISingUpFormShape } from '../types';

const tooShortMessage = 'минимальная длина - 8 символов';
const tooLongMessage = 'максимальная длина - 40 символов';

export const schema: yup.SchemaOf<ISingUpFormShape> = yup.object().shape({
    name: yup
        .string()
        .min(5, tooShortMessage)
        .max(40, tooLongMessage)
        .required('минимальная длина — 2 символов'),
    email: yup
        .string()
        .email()
        .required('Поле email обязательно для заполнения'),
    password: yup
        .string()
        .min(8, tooShortMessage)
        .max(16, tooLongMessage)
        .required('*'),
    confirmPassword: yup
        .string()
        .oneOf([yup.ref('password')], 'Пароли должны совпадать')
        .required('Поле confirmPassword обязательно для заполнения'),
});

export interface ISingUp extends Omit<ISingUpFormShape, 'confirmPassword'>{}

