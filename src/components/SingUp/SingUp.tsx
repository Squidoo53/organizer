import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { Link } from 'react-router-dom';
import { schema } from './config';
import { Input } from './Input';
import '../../theme/styles/index.scss';
import { useSingUp } from '../../hooks/useSingUp';
import { ISingUpFormShape } from '../types';

export const SingUp: React.FC = () => {
    const singUp = useSingUp();

    const form  = useForm({
        mode:     'onTouched',
        resolver: yupResolver(schema),
    });

    const onSubmit = form.handleSubmit(async (data: ISingUpFormShape) => {
        const { confirmPassword, ...newUser } = data;
        await singUp.mutateAsync(newUser);
        form.reset();
    });

    return (
        <main>
            <section className = 'sign-form'>
                <form onSubmit = { onSubmit } className = 'form'>
                    <fieldset disabled = { singUp.isLoading }>
                        <h1 className = 'legend'>Регистрация</h1>
                        <div className = 'inputBox'>
                            <Input
                                placeholder = 'Имя и Фамилия'
                                error = { form.formState.errors.name }
                                register = { form.register('name') } />
                            <Input
                                placeholder = 'Электропочта'
                                error = { form.formState.errors.email }
                                register = { form.register('email') } />
                            <Input
                                placeholder = 'Пароль'
                                type = 'password'
                                error = { form.formState.errors.password }
                                register = { form.register('password') } />
                            <Input
                                placeholder = 'Подтверждение пароля'
                                type = 'password'
                                error = { form.formState.errors.confirmPassword }
                                register = { form.register('confirmPassword') } />
                            <button type = 'submit' className = 'button-login'>Заригестрироватся</button>
                        </div>
                    </fieldset>
                    <p className = 'loginBtn'>Перейти к <Link to = '/singup' className = 'linkBtn'> логину.</Link></p>
                </form>
            </section>
        </main>

    );
};
