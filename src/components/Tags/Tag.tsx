import { MouseEvent } from 'react';
import { ITagModel } from '../../types/ITagModel';
import { TagWrapper } from '../styled/tags';

export interface ITagPropTypes extends ITagModel {
    isSelected?: boolean;
    handleClick?: (event: MouseEvent<HTMLSpanElement>) => void;
}


export const Tag: React.FC<ITagPropTypes> = ({
    bg, color, name, isSelected, handleClick,
}) => {
    const selected = isSelected ? 'selected' : '';

    return (
        <TagWrapper
            className = { `${selected}` }
            onClick = { handleClick }
            bg = { bg }
            color = { color }>
            { name }
        </TagWrapper>
    );
};

