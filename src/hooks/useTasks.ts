import { useDispatch, useSelector } from 'react-redux';
import { ITasksModel } from '../types/ITasksModel';
// Core

// Instruments
import {
    getIsTaskFormOpen, getIsTasksLoading, getSelectedTask, getTaskList,
} from '../lib/redux/selectors';
import { fetchTasksAsync, tasksActions } from '../lib/redux/actions';

export const useTasks = () => {
    const dispatch = useDispatch();

    const taskList = useSelector(getTaskList);
    const selectedTask = useSelector(getSelectedTask);
    const isTaskFormOpen = useSelector(getIsTaskFormOpen);
    const isTasksLoading = useSelector(getIsTasksLoading);

    const fetchTasks = () => dispatch(fetchTasksAsync());
    const updateTaskList = (tasks: ITasksModel[]) => dispatch(tasksActions.updateTaskList(tasks));
    const setSelectedTask = (task: ITasksModel | null) => {
        dispatch(tasksActions.setSelectedTask(task));
    };
    const setTaskFormOpen = (isOpen: boolean) => dispatch(tasksActions.setIsTaskFormOpen(isOpen));

    return {
        fetchTasks,
        isTasksLoading,
        taskList,
        selectedTask,
        isTaskFormOpen,
        updateTaskList,
        setSelectedTask,
        setTaskFormOpen,
    };
};
