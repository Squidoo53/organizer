import { useDispatch, useSelector } from 'react-redux';
import { fetchProfileAsync, profileActions } from '../lib/redux/actions';
import { getIsLoading, getProfile } from '../lib/redux/selectors';
import { IProfile } from '../types/IProfileModel';

export const useGetProfile = () => {
    const dispatch = useDispatch();

    const user = useSelector(getProfile);
    const isLoading = useSelector(getIsLoading);

    const fetchUserProfile = () => dispatch(fetchProfileAsync());
    const setUserProfile = (value: IProfile | null) => dispatch(profileActions.setProfile(value));


    return {
        user,
        isLoading,
        fetchUserProfile,
        setUserProfile,
    };
};

