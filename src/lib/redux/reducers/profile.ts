import { AnyAction } from 'redux';
import { IProfile } from '../../../types/IProfileModel';
import { profileTypes } from '../types';

export interface IProfileState {
    user: IProfile | null;
    isLoading: boolean;
}

const initialState: IProfileState = {
    user:      null,
    isLoading: false,
};

export const profileReducer = (state = initialState, action: AnyAction) => {
    switch (action.type) {
        case profileTypes.SET_PROFILE: {
            return {
                ...state,
                user: action.payload,
            };
        }

        case profileTypes.IS_LOADING: {
            return {
                ...state,
                isLoading: action.payload,
            };
        }

        default: {
            return state;
        }
    }
};
