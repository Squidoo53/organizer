import { toast } from 'react-toastify';
import { Dispatch } from 'redux';
import { api } from '../../../api';
import { IProfile } from '../../../types/IProfileModel';
import { profileTypes } from '../types';

export const profileActions = Object.freeze({
    setProfile: (user: IProfile | null) => {
        return {
            type:    profileTypes.SET_PROFILE,
            payload: user,
        };
    },

    setIsLoading: (value: boolean) => {
        return {
            type:    profileTypes.IS_LOADING,
            payload: value,
        };
    },
});

export const fetchProfileAsync = () => async (dispatch: Dispatch) => {
    try {
        dispatch(profileActions.setIsLoading(true));

        const user = await api.profile.fetch();

        dispatch(profileActions.setProfile(user));
    } catch {
        toast.error('Такого пользователя нет');
    } finally {
        dispatch(profileActions.setIsLoading(false));
    }
};
