// Core
import { combineReducers } from 'redux';

// Reducers
import {
    authReducer as auth,
    profileReducer as profile,
    tagsReducer as tags,
    tasksReducer as tasks,
} from '../reducers';


export const rootReducer = combineReducers({
    auth,
    profile,
    tags,
    tasks,
});
