import { useTasks } from '../hooks/useTasks';
import { ManagerOpen } from '../components/taskOpen/taskManagerOpen';
import { TasksList } from '../components/TasksList/TasksList';
import { Nav } from '../components/Nav/Nav';

export const TaskManager: React.FC = () => {
    const {
        taskList, isTaskFormOpen, setTaskFormOpen, setSelectedTask, isTasksLoading,
    } = useTasks();

    const openTaskForm = () => {
        setSelectedTask(null);
        setTaskFormOpen(true);
    };


    return (
        <div>
            <Nav />
            <main>
                <div className = 'controls'>
                    <a
                        className = 'button-create-task'
                        onClick = { openTaskForm }>
                    Новая задача
                    </a>

                </div>
                <div className = 'wrap' >
                    <div className = { `list ${!taskList?.length && !isTasksLoading ? 'empty' : ''}` }>
                        <TasksList />
                    </div>


                    { isTaskFormOpen === true ? <ManagerOpen /> : '' }

                </div>

            </main>
        </div>

    );
};
