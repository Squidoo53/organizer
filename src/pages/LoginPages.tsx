import React from 'react';
import { Login } from '../components/Login/login';
import { Nav } from '../components/Nav/Nav';


const LoginPage: React.FC = () => {
    return (
        <div>
            <Nav />
            <Login />
        </div>
    );
};

export default LoginPage;
