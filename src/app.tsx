// Core
import { FC } from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';
import { ToastContainer, Slide } from 'react-toastify';
import { HomePages } from './pages/HomePages';


// Components
import LoginPages from './pages/LoginPages';
import SingUpPage from './pages/SingUpPages';
import { TaskManager } from './pages/TaskManager';

// Instruments


export const App: FC = () => {
    return (
        <>
            <ToastContainer newestOnTop transition = { Slide } />
            <div className = 'App'>
                <Routes>
                    <Route path = '/' element = { <HomePages /> }></Route>
                    <Route path = '/login' element = { <LoginPages /> }></Route>
                    <Route path = '/singup' element = { <SingUpPage /> }></Route>
                    <Route path = '/task-manager' element = { <TaskManager /> }></Route>


                    <Route>
                        <Route path = '*'  element = { <Navigate to = '/' replace /> } />
                    </Route>
                </Routes>
            </div>
        </>
    );
};

